package main

import (
	"bytes"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

var /* const */ userReg = regexp.MustCompile(`@[a-zA-Z0-9_-]+`) //nolint

type botConf struct {
	sd map[string][]string // scum definitions - content of the files of the subdir scum
	bd map[string][]string // blades nsc definitions
}

// messageIteration is a container struct to pass on session and message
// required to send back an answer like this: s.ChannelMessageSend(m.ChannelID, ...... ).
type bot struct {
	s *discordgo.Session       // s.ChannelMessageSend is the most useful function
	m *discordgo.MessageCreate // m.ChannelID is where to send the message
}

func (b *bot) print(m string) {
	if m != "" {
		logerr(b.s.ChannelMessageSend(b.m.ChannelID, m))
	}
}

func main() {
	token, set := os.LookupEnv("BOTTOKEN")
	if !set {
		log.Fatalln("no BOTTOKEN ENV found, please set it")
	}
	var cf botConf
	cf.sd = slurpFiles("scum")
	cf.bd = slurpFiles("blades")

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + token)
	must(err)

	// Open a websocket connection to Discord and begin listening.
	dg.AddHandler(cf.message)

	must(dg.Open())
	defer dg.Close()

	log.Println("Bot up and running")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, os.Interrupt)
	<-sc
	log.Println("Bye bye you random world")
}

// message is listening on any channel that the autenticated bot has access to.
// this functions acts on every chat message that is incoming.
func (c *botConf) message(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID { // ignore all bot message
		return
	}
	b := bot{s: s, m: m}
	postID := m.Author.Username
	serverMember, err := s.GuildMember(m.GuildID, m.Author.ID)
	if err == nil && serverMember.Nick != "" {
		postID = serverMember.Nick
	}
	lines := strings.Split(m.Content, "|")
	for _, line := range lines {
		if userReg.MatchString(line) {
			postID = strings.Replace(userReg.FindAllString(line, 1)[0], "@", "", 1)
			line = userReg.ReplaceAllString(line, "")
		}
		switch {
		case strings.EqualFold(strings.ToLower(line), "strings"):
			b.print(b.callouts())
		case strings.HasPrefix(strings.ToLower(line), "heartsnsc"):
			b.print(generateMonsterheartsNSC(line))
			return
		case len(line) > 30:
			return
		case strings.HasPrefix(line, "http"):
			return
		case line == "version":
			b.print(buildsha())
		case line == "coin":
			b.print(coinFlip())
		case strings.EqualFold(line, "bnsc"):
			b.print(bladesNonPlayerCharacter(c.bd))
		case strings.EqualFold(line, "scum"):
			b.print(scumNonPlayerCharacter(c.sd))
		case strings.HasPrefix(strings.ToLower(line), "heart"):
			b.print(heart(line, postID))
		case strings.HasPrefix(line, "prost"):
			b.print(prost(line))
		case fateRegexMatch(line):
			b.print(workFate(line, postID))
		case strings.HasPrefix(strings.ToLower(line), "dark"):
			b.print(workDark(line, postID))
		case monsterheartsActions(line):
			b.print(mhActionMod(line, postID))
		case strings.EqualFold(line, "monsterhearts"):
			b.print(monsterheartshelp())
		default:
			b.print(workDice(line, postID))
		}
	}
}

// coinFlip just hands back Head or Tail.
func coinFlip() string {
	flip := rand.Intn(2) + 1
	if flip == 1 {
		return "HEAD"
	}
	return "TAIL"
}

// abs is a helper function to always get the positive number
// from the given integer.
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// normalize converts expressions like 3w6 to w6+w6+w6
// It is the main function to convert any expression of dice
// into the required rolles.
func normalize(working string) (cleaned string) {
	working = strings.Replace(working, " ", "", -1)
	working = strings.ToLower(working)
	working = germanize(working)

	// Regex + RegexReplace to normalize string
	src := []byte(working)
	search := regexp.MustCompile(`[+-]?\d+w\d+`)

	src = search.ReplaceAllFunc(src, func(s []byte) []byte {
		num := strings.Split(string(s), "w")
		count, _ := strconv.Atoi(num[0])
		if count < 0 {
			total := strings.Repeat("-w"+num[1], abs(count))

			return []byte(total)
		}
		total := strings.Repeat("+w"+num[1], count)

		return []byte(total)
	})
	return string(bytes.Trim(src, "\x00"))
}

// workDice is for all normal rolls, that is all requests of 3w8 + 2w33 ...
// There are limits imposed on the input
// This function is compute heavy and therefore should be called last.
func workDice(working, postID string) string {
	displayResult := false
	output := ""
	working = normalize(working)
	lazy := 0
	withW := regexp.MustCompile(`^w\d+$`)
	onlyN := regexp.MustCompile(`^\d+$`)
	math := regexp.MustCompile(`[+-]`)
	parts := tokenize(working)
	lastmath := "+"
	if len(parts) > 10001 {
		return postID + "# maximal 10000 dice in 1 action"
	}
	for _, v := range parts {
		switch {
		case math.MatchString(v):
			lastmath = v
		case withW.MatchString(v):
			displayResult = true
			v = strings.Replace(v, "w", "", -1)
			size, _ := strconv.Atoi(v)
			diceroll := rand.Intn(size) + 1
			output += " " + lastmath + "(" + v + ") " + strconv.Itoa(diceroll)
			if lastmath == "+" {
				lazy += diceroll
			} else {
				lazy -= diceroll
			}
		case onlyN.MatchString(v):
			output += " " + lastmath + v
			add, err := strconv.Atoi(v)
			if err == nil {
				if lastmath == "+" {
					lazy += add
				} else {
					lazy -= add
				}
			}
		default:
			output += " " + v
		}
	}
	if len(parts) > 1 {
		output += "\n# " + strconv.Itoa(lazy)
	}
	if len(parts) > 100 {
		output = "#" + strconv.Itoa(lazy)
	}
	if displayResult {
		return postID + "#" + output
	}
	return ""
}

// tokenize works on the normalized data to slice it into
// tokens to be used for randomized dice rolls.
func tokenize(input string) (output []string) {
	data := strings.NewReader(input)
	maxLength := data.Len()
	number := regexp.MustCompile(`\d`)
	dice := regexp.MustCompile(`w`)
	math := regexp.MustCompile(`[+-]`)

	var token string

	for i := 0; i <= maxLength; i++ {
		b, _ := data.ReadByte()
		switch {
		case number.MatchString(string(b)):
			token += string(b)
		case dice.MatchString(string(b)):
			token += string(b)
		case math.MatchString(string(b)):
			output = append(output, token, string(b))
			token = ""
		default:
			// End of string reached
			output = append(output, token)
			token = ""
		}
	}
	return
}

// successLevel adds chat representation to a in the dark roll result
// marking it Red, yellow, Green and Green++.
// nolint - max and nesting out there
func successLevel(rolls []int, skill bool) string {
	max := 0
	if skill { // nolint -- nestif it would be hard to reduce
		for _, v := range rolls {
			if max == 4 {
				continue
			}
			if v == 6 {
				max += 2
			}
		}
		if max == 0 {
			for _, v := range rolls {
				if v == 4 || v == 5 {
					max = 1
					break
				}
			}
		}
	} else {
		// we get them sorted already, therefore
		switch rolls[0] {
		case 1, 2, 3:
			max = 0
		case 4, 5:
			max = 1
		case 6:
			max = 2
		}
	}

	switch max {
	case 0:
		return "```diff\n- ROLL\n```"
	case 1:
		return "```autohotkey\n% ROLL%\n```"
	case 2:
		return "```diff\n+ ROLL\n```"
	case 4:
		return "```diff\n+ ROLL !!!\n```"
	default:
		return "ROLL"
	}
}

// germanize substitutis d for w if a number follows. Allowing for
// 3d4 to be like 3w4.
func germanize(input string) (output string) {
	data := strings.NewReader(input)
	maxLength := data.Len()

	number := regexp.MustCompile(`\d`)
	lastpart := ""
	for i := 0; i <= maxLength; i++ {
		b, _ := data.ReadByte()
		if lastpart == "d" && number.MatchString(string(b)) {
			output += "w"
			lastpart = string(b)
		} else {
			output += lastpart
			lastpart = string(b)
		}
	}
	output += lastpart
	return
}

// d6 is mostly rand.Intn(6)+1 but allows for loaded dice.
func d6(q ...string) int {
	cycles := 0
	updown := "up"
	if len(q) != 0 {
		if strings.HasPrefix(q[0], "<") {
			updown = "down"
			q[0] = strings.Replace(q[0], "<", "", -1)
		}
		cycles, _ = strconv.Atoi(q[0])
	}
	rolls := []int{}
	if cycles == 0 {
		return rand.Intn(6) + 1
	}

	for i := -1; i <= cycles; i++ {
		rolls = append(rolls, rand.Intn(6)+1)
	}

	sort.Ints(rolls)
	if updown == "down" {
		return rolls[0]
	}
	return rolls[len(rolls)-1]
}

// d20 is mostly rand.Intn(20)+1 but allows for loaded dice.
func d20(q ...string) int {
	cycles := 0
	updown := "up"
	if len(q) != 0 {
		if strings.HasPrefix(q[0], "<") {
			updown = "down"
			q[0] = strings.Replace(q[0], "<", "", -1)
		}
		cycles, _ = strconv.Atoi(q[0])
	}
	rolls := []int{}
	if cycles == 0 {
		return rand.Intn(20) + 1
	}

	for i := -1; i <= cycles; i++ {
		rolls = append(rolls, rand.Intn(20)+1)
	}

	sort.Ints(rolls)
	if updown == "down" {
		return rolls[0]
	}
	return rolls[len(rolls)-1]
}

// logerr, prints error if present, exists to please the linter.
func logerr(msg *discordgo.Message, err error) {
	if err != nil {
		log.Println(msg, err)
	}
}
