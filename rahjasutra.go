// nolint -- this is file is just too aweful
package main

import (
	"math/rand"
	"strings"
)

type worker struct {
	genderroll         int
	q                  string // loaded dice mod
	p                  string // loaded dice mod
	quality            int
	geschlecht         string
	penis              string
	brueste            string
	vagina             string
	schamhaarmenge     string
	schamhaarfrisur    string
	praf               string
	frisur             string
	maxsonder          int
	erfahrung          string
	orgien             string
	ausbildung         string
	arbeitseinstellung string
	extras             []string
	vorteile           []string
}

func prost(inbound string) string {
	w := genderquality(inbound)

	physics(&w)
	haarVerteilung(&w)
	frisurset(&w)
	preference(&w)
	skilllevel(&w)
	orgienskill(&w)
	trainingskill(&w)
	arbeitseinsatz(&w)
	setExtras(&w)
	vorteileSet(&w)

	output := "```"
	output += w.geschlecht + "\n"
	output += "Steht auf         : " + w.praf + "\n"
	output += "Ausbildung        : " + w.ausbildung + "\n"
	output += "Erfahrung         : " + w.erfahrung + "\n"
	output += "Orgienerfahrung   : " + w.orgien + "\n"
	output += "Arbeitseinstellung: " + w.arbeitseinstellung + "\n"
	output += "Haare             : " + w.frisur + "\n"
	output += "Schamhaar         : " + w.schamhaarmenge + "\n"
	if w.schamhaarmenge != "einen Dschungel" && w.schamhaarmenge != "Aranisch (glattrasiert)" {
		output += "schamhaarfrisur   : " + w.schamhaarfrisur + "\n"
	}
	for _, v := range w.extras {
		output += "\t Vor/Nachteil   : " + v + "\n"
	}
	for _, v := range w.vorteile {
		output += "\t Sonderfähigkeit: " + v + "\n"
	}
	output += "```"
	return output
}

func dup(slice []string) bool {
	max := len(slice)
	for i, v := range slice {
		if i == max-1 {
			continue
		}
		if v == slice[max-1] {
			return true
		}
	}
	return false
}

func stufe() (stufe string) {
	switch d6() {
	case 1, 2, 3, 4:
		stufe = "Stufe I"
	case 5, 6:
		stufe = "Stufe II"
	}
	return
}

func praktik() (prak string) {
	switch d20() {
	case 1:
		prak = "Verbalerotik"
	case 2, 3, 4:
		prak = "Küssen & Lecken"
	case 5, 6, 7:
		prak = "Reiben & Streicheln"
	case 8, 9:
		prak = "Fingerspiele"
	case 10:
		prak = "Mammalverkehr"
	case 11, 12, 13:
		prak = "Oralverkehr"
	case 14, 15, 16, 17:
		prak = "Coitus"
	case 18, 19, 20:
		switch d6() {
		case 1:
			prak = "Analverkehr"
		case 2:
			prak = "Faustverkehr"
		case 3:
			prak = "Fesselspiele"
		case 4:
			prak = "Harte Gangart"
		case 5:
			prak = "Samenspiele"
		case 6:
			prak = "Wasserspiele"
		}
	}
	return prak
}

// Setting the generic qualities
func genderquality(request string) (w worker) {
	w = worker{}
	switch request {
	case "prost f+":
		w.genderroll = 9
		w.q = "<2"
		w.p = "2"
		w.quality = 2
	case "prost f":
		w.genderroll = 9
	case "prost m+":
		w.genderroll = 11
		w.q = "<2"
		w.p = "2"
		w.quality = 2
	case "prost m":
		w.genderroll = 11
	case "prost":
		w.genderroll = d20()
	}
	return
}

// Generic physics of the sex worker
func physics(w *worker) {
	if w.genderroll > 10 {
		w.geschlecht = "Mann"
		switch d6(w.p) + d6(w.p) + d6(w.p) {
		case 3, 4, 5:
			w.penis = "sehr kleinen"
		case 6, 7, 8:
			w.penis = "kleinen"
		case 9, 10, 11, 12:
			w.penis = "normalen"
		case 13, 14, 15:
			w.penis = "großen"
		case 16, 17, 18:
			w.penis = "sehr großen"
		}
		w.geschlecht += " mit einem " + w.penis + " Penis"
	} else {
		w.geschlecht = "Frau"
		switch d6() + d6() + d6(w.p) {
		case 3, 4, 5:
			w.brueste = "sehr kleinen"
		case 6, 7, 8:
			w.brueste = "kleinen"
		case 9, 10, 11, 12:
			w.brueste = "mittleren"
		case 13, 14, 15:
			w.brueste = "großen"
		case 16, 17, 18:
			w.brueste = "sehr großen"
		}
		switch d6(w.q) + d6(w.q) + d6(w.q) {
		case 3, 4, 5:
			w.vagina = "sehr kleiner"
		case 6, 7, 8:
			w.vagina = "kleiner"
		case 9, 10, 11, 12:
			w.vagina = "normaler"
		case 13, 14, 15:
			w.vagina = "großer"
		case 16, 17, 18:
			w.vagina = "sehr großer"
		}
		w.geschlecht += " mit " + w.brueste + " Brüsten und " + w.vagina + " Vagina"
	}
}

func haarVerteilung(w *worker) {
	switch d6(w.q) + d6(w.q) + d6(w.q) {
	case 3, 4, 5:
		w.schamhaarmenge = "Kein Schamhaar"
	case 6, 7, 8:
		w.schamhaarmenge = "Wenig Schamhaar"
	case 9, 10, 11, 12:
		w.schamhaarmenge = "Normales Schamhaar"
	case 13, 14, 15:
		w.schamhaarmenge = "viel Schamhaar"
	case 16, 17, 18:
		w.schamhaarmenge = "einen Dschungel"
	}

	switch d20(w.q) {
	case 1, 2, 3, 4, 5:
		w.schamhaarfrisur = "Aranisch (glattrasiert)"
	case 6, 7:
		w.schamhaarfrisur = "Streifen"
	case 8, 9:
		w.schamhaarfrisur = "Dreieck"
	case 10:
		w.schamhaarfrisur = "Rondrablitz"
	case 11:
		w.schamhaarfrisur = "Ein Buchstabe"
	case 12:
		w.schamhaarfrisur = "Hesindeschlange"
	case 13:
		w.schamhaarfrisur = "Pentagramm"
	case 14:
		w.schamhaarfrisur = "Kaiserbart"
	case 15:
		w.schamhaarfrisur = "Ein anders Symbol"
	case 16, 17, 18, 19, 20:
		w.schamhaarfrisur = "unbehandeltes Schamhaar"
	}
}

func frisurset(w *worker) {
	switch d20() {
	case 1, 2:
		w.frisur = "Pferdeschwanz"
	case 3, 4:
		w.frisur = "Zopf"
	case 5:
		w.frisur = "zwei Zöpfe"
	case 6, 7:
		w.frisur = "Pony"
	case 8:
		w.frisur = "Landor Gerrano-Gedächtnisfrisur (eine Art Pagenschnitt)"
	case 9, 10, 11, 12:
		w.frisur = "offenes, langes Haar"
	case 13:
		w.frisur = "Glatze"
	case 14:
		w.frisur = "sehr kurzes, stoppeliges Haare"
	case 15:
		w.frisur = "offenes Haar bis zu den Schultern"
	case 16:
		w.frisur = "Seitenscheitel"
	case 17:
		w.frisur = "Mittelscheitel"
	case 18:
		w.frisur = "kunstvoll hochgestecktes Haar"
	case 19:
		w.frisur = "Lockenkopf"
	case 20:
		w.frisur = "welliges Haar"
	}
}

func preference(w *worker) {
	switch d20(w.q) {
	case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
		w.praf = "Nur das andere Geschlecht"
	case 13, 14, 15:
		w.praf = "Beide Geschlechter"
	case 16, 17, 18, 19, 20:
		w.praf = "Nur das eigene Geschlecht"
	}
}

// skilllevel is setting the general talent
func skilllevel(w *worker) {
	w.maxsonder = rand.Intn(3) + 1 + w.quality
	switch d6(w.p) {
	case 1, 2, 3:
		w.erfahrung = "Durchschnittlich"
	case 4, 5:
		w.erfahrung = "Erfahren"
	case 6:
		w.maxsonder++
		w.erfahrung = "Kompetent"
	}
}

func orgienskill(w *worker) {
	switch d20(w.p) {
	case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15:
		w.orgien = "Orgienfrischling"
	case 16, 17, 18:
		if strings.HasPrefix(w.geschlecht, "Frau") {
			w.orgien = "Orgienlöwin"
		} else {
			w.orgien = "Orgienlöwe"
		}
	case 19, 20:
		if strings.HasPrefix(w.geschlecht, "Frau") {
			w.orgien = "Orgienveteranin"
		} else {
			w.orgien = "Orgienveteran"
		}
	}
}

func trainingskill(w *worker) {
	switch d20() {
	case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15:
		if strings.HasPrefix(w.geschlecht, "Frau") {
			w.ausbildung = "Hure"
		} else {
			w.ausbildung = "Lustknabe"
		}
	case 16, 17, 18:
		if strings.HasPrefix(w.geschlecht, "Frau") {
			w.ausbildung = "Kurtisane"
		} else {
			w.ausbildung = "Gesellschafter"
		}
	case 19, 20:
		if strings.HasPrefix(w.geschlecht, "Frau") {
			w.ausbildung = "Mätresse"
		} else {
			w.ausbildung = "Favorit"
		}
		w.maxsonder++
	}
}

func arbeitseinsatz(w *worker) {
	switch d20(w.p) {
	case 1:
		w.arbeitseinstellung = "mit offensichtlicher Langeweile"
	case 2:
		w.arbeitseinstellung = "mit gespielter Hingabe"
	case 3:
		w.arbeitseinstellung = "ohne ihre Abneigung zu verbergen"
	case 4:
		w.arbeitseinstellung = "sichtlich Nervös"
	case 5:
		w.arbeitseinstellung = "(sehr) ungeschickt"
	case 6:
		w.arbeitseinstellung = "völlig übermüdet"
	case 7:
		w.arbeitseinstellung = "äußerst lustlos"
	case 8:
		w.arbeitseinstellung = "geschäftstüchtig aber ohne Leidenschaft"
	case 9:
		w.arbeitseinstellung = "ungeschickt aber enthusiastisch"
	case 10:
		w.arbeitseinstellung = "äußerst professionell"
	case 11:
		w.arbeitseinstellung = "zu dessen vollster Zufriedenheit"
	case 12:
		w.arbeitseinstellung = "(sehr) geschickt"
	case 13:
		w.arbeitseinstellung = "sehr spielerisch"
	case 14:
		w.arbeitseinstellung = "abwesend bis apathisch"
	case 15:
		w.arbeitseinstellung = "äußerst kapriziös"
	case 16:
		w.arbeitseinstellung = "mit rahjagefälliger Hingabe"
	case 17:
		w.arbeitseinstellung = "als liebe sie ihn über alles"
	case 18:
		w.arbeitseinstellung = "sehr zärtlich"
	case 19:
		w.arbeitseinstellung = "inbrünstig"
	case 20:
		w.arbeitseinstellung = "reserviert"
	}
}

func setExtras(w *worker) {
	max := rand.Intn(3) + 1 + w.quality
	var extras []string
	for i := 1; i <= max; i++ {
		switch d20(w.q) {
		case 1:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				extras = append(extras, "Ausdauernde Liebhaberin")
			} else {
				extras = append(extras, "Ausdauernder Liebhaber")
			}
		case 2:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				extras = append(extras, "Begabte Aufreißerin")
			} else {
				extras = append(extras, "Begabter Aufreißer")
			}
		case 3:
			extras = append(extras, "Erotische Ausstrahlung")
		case 4:
			extras = append(extras, "Erotische Stimme")
		case 5:
			extras = append(extras, "Freudenfluss")
		case 6:
			extras = append(extras, "Große Blase")
		case 7:
			extras = append(extras, "Gutaussehend: "+stufe())
		case 8:
			extras = append(extras, "Guter Geschmack")
		case 9:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				extras = append(extras, "Hypnotische(s) Brüste / Gesäß")
			} else {
				extras = append(extras, "Hypnotische(s) Gesäß / Gemächt")
			}
		case 10:
			extras = append(extras, "Levthangeküsst: "+stufe())
		case 11:
			extras = append(extras, "Potent")
		case 12:
			extras = append(extras, "Rahjageküsst")
		case 13:
			extras = append(extras, "Resistenz gegen Geschlechtskrankheiten")
		case 14:
			extras = append(extras, "Vorliebe "+praktik()+": "+stufe())
		case 15:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				extras = append(extras, "Wohlgeformte Brüste / Wunderschöne Vagina")
			} else {
				extras = append(extras, "Stattlicher Penis / Traumhafter Po")
			}
		case 16, 17:
			extras = append(extras, "Abneigung "+praktik()+": "+stufe())
		case 18:
			extras = append(extras, "Hässlich "+stufe())
		case 19:
			extras = append(extras, "Schlechter Geschmack")
		case 20:
			extras = append(extras, "Unerotische Stimme")
		}
		if dup(extras) {
			extras = extras[:len(extras)-1]
			i--
		}
	}
	w.extras = extras
}

func vorteileSet(w *worker) {
	var vorteile []string
	for j := 1; j <= w.maxsonder; j++ {
		switch d20() {
		case 1:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Abschleppspezialistin")
			} else {
				vorteile = append(vorteile, "Abschleppspezialist")
			}
		case 2:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Einfühlsame Verehrerin")
			} else {
				vorteile = append(vorteile, "Einfühlsamer Verehrer")
			}
		case 3:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Erotikmasseurin")
			} else {
				vorteile = append(vorteile, "Erotikmasseur")
			}
		case 4:
			vorteile = append(vorteile, "Erregende Worte")
		case 5:
			vorteile = append(vorteile, "Flinke Finger")
		case 6:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Flüssigkeitsliebhaberin")
			} else {
				vorteile = append(vorteile, "Flüssigkeitsliebhaber")
			}
		case 7:
			vorteile = append(vorteile, "Geschickte Zunge")
		case 8:
			vorteile = append(vorteile, "Gruppensex-Veteran: "+stufe())
		case 9:
			vorteile = append(vorteile, "Gute Technik: "+praktik())
		case 10:
			vorteile = append(vorteile, "Lustempathiet")
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Flüssigkeitsliebhaberin")
			} else {
				vorteile = append(vorteile, "Flüssigkeitsliebhaber")
			}
		case 11:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Lustspenderin")
			} else {
				vorteile = append(vorteile, "Lustspender")
			}
		case 12:
			vorteile = append(vorteile, "Mal hart, mal zart")
		case 13:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Orgiastische Lustspenderin")
			} else {
				vorteile = append(vorteile, "Orgiastischer Lustspender")
			}
		case 14:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Orgienlöwin: "+stufe())
			} else {
				vorteile = append(vorteile, "Orgienlöwe: "+stufe())
			}
		case 15:
			vorteile = append(vorteile, "Rahjasutra-Kenntnisse")
		case 16:
			vorteile = append(vorteile, "Samentausch")
		case 17:
			vorteile = append(vorteile, "Verführungskunst")
		case 18:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Versierte Sadistin")
			} else {
				vorteile = append(vorteile, "Versierter Sadist")
			}
		case 19:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Versierte Masochistin")
			} else {
				vorteile = append(vorteile, "Versierter Masochist")
			}
		case 20:
			if strings.HasPrefix(w.geschlecht, "Frau") {
				vorteile = append(vorteile, "Zärtliche Liebhaberin")
			} else {
				vorteile = append(vorteile, "Zärtlicher Liebhaber")
			}
		}
		if dup(vorteile) {
			vorteile = vorteile[:len(vorteile)-1]
			j--
		}
	}
	w.vorteile = vorteile
}
