package main

import (
	"regexp"
	"sort"
	"strconv"
	"strings"
)

// workDark works on blades in the dark rolls. If the keyword dark
// is not at the start of the chatline it will not act but return
// the caller, result indicates on true that it has worked on the message.
func workDark(chat, postID string) string {
	risky := strings.HasSuffix(strings.ToLower(chat), "r")
	skillroll := true
	countString := regexp.MustCompile(`\d{1,3}`).FindString(chat)
	maxRoll, _ := strconv.Atoi(countString)
	if maxRoll == 0 {
		skillroll = false // Oh dear - setting max to 2 and picking the smallest
		maxRoll = 2
	}
	var results []int
	for i := 0; i < maxRoll; i++ {
		results = append(results, d6())
	}
	sort.Ints(results)
	text := successLevel(results, skillroll)
	chat = "dark :"
	gambit := false
	for _, v := range results {
		if v == 6 {
			gambit = true
		}
		chat += strconv.Itoa(v)
		chat += " "
	}
	if gambit && risky {
		chat += " and you get a new Gambit. Yay!"
	}
	return postID + "#" + strings.Replace(text, "ROLL", chat, 1)
}
