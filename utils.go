package main

import (
	"log"
	"math/rand"
	"os"
	"runtime/debug"
	"strings"
)

// buildsha() uses golang 1.18 build debug information to get the last
// commit sha, wraper call around debug.ReadBuildInfo.
func buildsha() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		log.Fatalln("Build info not found")
	}
	for _, v := range info.Settings {
		if v.Key == "vcs.revision" {
			return "discorddice " + v.Value
		}
	}
	return "failed to extract from binary"
}

// slurpFiles reads in a folder and all txt files inside that
// folder will become []string of the map with that files name.
func slurpFiles(folder string) map[string][]string {
	item, err := os.ReadDir(folder)
	must(err)
	sd := make(map[string][]string)
	for _, entry := range item {
		if !strings.HasSuffix(entry.Name(), ".txt") {
			continue
		}
		b, err := os.ReadFile(folder + "/" + entry.Name())
		must(err)
		var content []string
		for _, line := range strings.Split(string(b), "\n") {
			for _, token := range strings.Split(line, ",") {
				token = strings.TrimSpace(token)
				if token == "" {
					continue
				}
				content = append(content, strings.TrimSpace(token))
			}
		}
		sd[strings.TrimSuffix(entry.Name(), ".txt")] = content
	}
	return sd
}

// nscLookup takes any []string and returns a random element of it.
func nscLookup(item []string) string {
	return item[rand.Intn(len(item))]
}

// must calls panic on all non-errors.
func must(err error) {
	if err != nil {
		panic(err)
	}
}
