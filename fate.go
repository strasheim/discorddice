package main

import (
	"math/rand"
	"regexp"
	"strconv"
	"strings"
)

var /* const  */ fateRegex = regexp.MustCompile(`^(fa?t?e?)(\+?\-?) ?(\d?)$`) //nolint

// fateRegexMatch is a wrapper for the higher level switch statement.
func fateRegexMatch(line string) bool {
	match := fateRegex.FindAllStringSubmatch(line, -1)
	return len(match) != 0
}

// workFate allows to roll on fate like expressions
// The fate dice system can be abstracted to a math function
// yet this would not give the results of each roll, just the
// outcome and therefore to preserve the feeling of fate
// this does roll the fate expression like fate dice on the table would do.
func workFate(line, postID string) string {
	match := fateRegex.FindAllStringSubmatch(line, -1)
	normalize := ""

	var result [4]int
	for i := range result {
		result[i] = rand.Intn(3) - 1
	}
	total := result[0] + result[1] + result[2] + result[3]

	for i, v := range match[0] {
		switch i {
		case 1:
			normalize += "fate "
		case 2:
			switch v {
			case "", "+":
				normalize += "+ "
			case "-":
				normalize += "- "
			}
		case 3:
			switch v {
			case "":
				normalize += "0"
			default:
				normalize += v
			}
		}
	}
	parts := strings.Split(normalize, " ")
	num, _ := strconv.Atoi(parts[2])
	if parts[1] == "+" {
		total += num
	} else {
		total -= num
	}

	answer := "[" + strconv.Itoa(result[0]) + "," + strconv.Itoa(result[1]) + "," + strconv.Itoa(result[2]) + "," + strconv.Itoa(result[3]) + "]" + parts[1] + parts[2] + " # **" + strconv.Itoa(total) + "**"
	return postID + "#" + answer
}
