### Ridiculous simple dice bot 

1. Build the go binary or download it from [here](https://gitlab.com/strasheim/discorddice/-/jobs/artifacts/master/download?job=discorddice)
1. Create an Bot in discord (the name you pick for the application will be the nickname in your chat)
    1. https://discordapp.com/developers/applications/me
    1. Make it a "chat bot"
    1. Get the application ID and enter the following link in a browser
    1. Create permissions for the bot: https://discordapp.com/oauth2/authorize?client_id={botID}&permissions=2048&scope=bot
    1. Save the auth token 
1. Start the binary with ./discorddice -t {authtoken}

An application ID you can use for testing is:

![Dev App ID](appid.png)


If you want to add the bot to another existing server re-run step 4.

#### How it works
Once invited to your server (step 4 from above, with for example the ID from the image), the bot reacts to all chat messages which contain:

* XwY + Z
* XwY + XwY + ... 
* XwY - Z ..
* Macros 
* XwY can also be XdY 
* XwY | XwY  

The bot reads the X if not present before the w(Wuerfel in German) it assume a 1 and calls rand on Y X times and adds Z to it.
Each dice is displayed on it's own. If more than 1 die is present it will print a second line with the sum. 
If it finds something like dX somewhere it will convert it into wX. 
* | is seen as split between rolls, to allow for many rolls in the same chat action
* @ will replace the username of the poster to the frist @<string> of the message 

This allows for example for:
* 4w6 @Attack | 3w6 @Defence 

to be resolved on the same line in a single chat message or macro

#### Limits
* The bot will not work on more then 10k dice per chat message.
* Chat messages which are longer than 30 characters are ignored. 

#### Special Dice
* coin  -> will result in ether head or tail
* fate  -> will perform a fate role, takes arguments like -2 or +3, shortcut f works as well
* dark  -> sorts output to work well with an "in the dark" settings. 

The special dice action are less generic and require no spaces in front of each word. The @ and | work yet the requirement for no-whitespaces is there as well. 

----

### Privicy Warning !!! 

If you use the developer ID from above, all your chat message are seen by the bot (how else would the bot react to those). There is no persistent logging on the server where the bot is running. However the logs only vanish from the server when the bot is restarted for any reason.

Please run your own instance of the bot, don't think the developement setup is up and running 24x7. 

---
* This tool wouldn't have been alive if it wasn't for the [golang API](https://github.com/bwmarrin/discordgo) for diccord
* The API is under [BSD3](https://github.com/bwmarrin/discordgo/blob/master/LICENSE) license
* The chatbot was inspired by the example pingpong bot from the API
* The Bot is under the MIT license

