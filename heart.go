package main

import (
	"math/rand"
	"regexp"
	"sort"
	"strconv"
)

// heart dice action on a single chat line.
func heart(chat, postID string) string {
	rollType := regexp.MustCompile(`[nrd]$`).FindString(chat)

	diceCount := regexp.MustCompile(`\d`).FindString(chat)
	num, _ := strconv.Atoi(diceCount)
	rolls := []int{}
	for i := 1; i <= num; i++ {
		rolls = append(rolls, rand.Intn(10)+1)
	}
	sort.Ints(rolls)
	if rollType == "r" {
		rolls = rolls[:len(rolls)-1]
	}
	if rollType == "d" {
		rolls = rolls[:len(rolls)-2]
	}

	output := ""
	for _, part := range rolls {
		output += strconv.Itoa(part)
		output += " "
	}
	return postID + "#" + output
}
