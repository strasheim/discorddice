package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Some globals, as those are easier to see and actual consts, but those type
// do not work in golang as const.
// nolint
var monsterHeartprefixes = []string{"turnon", "shutdown", "keepcool", "lashout", "runaway", "gazeinto"}
var mctext = map[string]string{
	"runaway":  runAway,
	"gazeinto": gazeIntoTheAbyss,
	"turnon":   turnOnSomeone,
	"shutdown": shutSomeoneDown,
	"keepcool": keepYourCool,
	"lashout":  lashOutPhysically,
}

const runAway = `> Run Away
When you **run away**, roll with **Volatile**. On a **10** up, you get away to a **safe place**. On a **7-9**, **you** get away **but** choose one:
* You run into something worse
* You cause a big scene
* You leave something behind
`
const gazeIntoTheAbyss = `> Gaze into the abyss
When you **gaze into the abyss**, name what you’re looking for and roll with **Dark**. On a **10** up, the abyss shows you lucid visions, **and** you take **1 Forward** to addressing them.  On a **7-9**, the abyss shows you **confusing and alarming visions**, but you get your answer nonetheless.
`

const turnOnSomeone = `> Turn someone on
When you turn someone on, roll with **Hot**. On a **10** up, gain a String on them **and** they choose a reaction from below. • On a **7-9**, **they** can either **give** you a String **or** choose one of the **reactions**.
* I give myself to you
* I promise something I think you want
* I get embarrassed and act awkward
`

const shutSomeoneDown = `> Shut someone down
When you shut someone down, roll with **Cold**. On a 10 up, **choose one** from below. • On a 7-9, **choose one** from below, but you come across **poorly**, and they give you a **Condition** in return.
* They lose a String on you
* If they have no Strings on you, gain one on them
* They gain a Condition
* You take 1 Forward
`

const keepYourCool = `> Kepp your cool
When you **keep your cool** and **act despite fear**, **name** what you’re **afraid of** and roll with **Cold**. On a **10** up, you keep your cool and **gain insight**: ask the **MC a question** about the situation and take **1 Forward** to acting on that information. • On a **7-9**, the **MC** will **tell** you how your actions would **leave you vulnerable**, and you can choose to **back down** or **go through with it**.
`

const lashOutPhysically = `> Leah out physically
When you **lash out physically**, roll with **Volatile**. On a **10** up, you **deal** them **harm**, and they choke up momentarily before they can react. • On a **7-9**, you harm them **but** choose one:
* They learn something about your true nature and gain a String on you
* The MC decides how bad the harm turns out
* You become your Darkest Self
`

const failureXP = `
> **Failure**

A result of 6 or lower is a **failure** and adversity nudge your character toward growth. A number of the Skins also have a move option which **gains experience**.

**Your move failed.**
`

const monsterHeartsSetting = `

Monsterhearts 2 is a tabletop role-playing game set in a world where players take on the roles of teenage monsters navigating the intense drama of high school life. The setting blends supernatural horror with coming-of-age themes, exploring love, identity, power, and vulnerability amidst a backdrop of emotional chaos and dark secrets. It’s steeped in personal drama, relationships, and the messy nature of growing up—complicated further by monstrous abilities and desires.

The character you create should include hints to their family. Do they have siblings, parents and so fourth. Do they have a nickname? Are they popular in class, nerds, dirtbags, metalheads, please use stereotypes for the task. 

The school is in germany. Please write in german and use the german schools as backdrops not the US. Unless said, the character is not a monster, just a human.
`

// monsterheartshelp gives out the commands that the bot would work on.
func monsterheartshelp() string {
	list := "## Monsterhearts Moves\n"
	for _, v := range monsterHeartprefixes {
		list += "* " + v + "\n"
	}
	list += "### Non-Move Actions\n"
	list += "* Pull Strings\n"
	list += "* Heal Harm\n"
	return list
}

// generateMonsterheartsNSC uses genAI to give a text about an NSC.
func generateMonsterheartsNSC(wish string) string {
	wish, _ = strings.CutPrefix(wish, "heartsnsc ")
	return generateNSCText(wish, monsterHeartsSetting)
}

// monsterheartsActions matches on the known prefixes.
func monsterheartsActions(line string) bool {
	lowerLine := strings.ToLower(line)
	for _, prefix := range monsterHeartprefixes {
		if strings.HasPrefix(lowerLine, prefix) {
			return true
		}
	}
	return false
}

// mhActionMod matches on the known prefixes.
func mhActionMod(line, postID string) (result string) {
	lowerLine := strings.ToLower(line)
	for _, prefix := range monsterHeartprefixes {
		if !strings.HasPrefix(lowerLine, prefix) {
			continue
		}
		clean, done := strings.CutPrefix(line, prefix)
		if !done {
			return ""
		}
		a, b, m := mhRoll(strings.TrimSpace(clean))
		result := a + b + m
		switch {
		case result < 7:
			return mctext[prefix] + failureXP + mhResultPrint(postID, a, b, m)
		default:
			return mctext[prefix] + mhResultPrint(postID, a, b, m)
		}
	}
	return ""
}

// mhRoll does the 2d6 + mod roll every Monsterheart roll does.
func mhRoll(mod string) (a, b, m int) {
	intMod, err := strconv.Atoi(mod)
	if err != nil {
		intMod = 0
	}
	return d6(), d6(), intMod
}

// mhResultPrint should show a clear roll outcome.
func mhResultPrint(who string, a, b, m int) string {
	result := bold(strconv.Itoa(a + b + m))
	return fmt.Sprintf("\n %s: [%s] = %s + %s + %s",
		bold(extractLastPart(who)),
		result,
		bold(strconv.Itoa(a)),
		bold(strconv.Itoa(b)),
		bold(strconv.Itoa(m)))
}

// bold does markdown bold changes to the string.
func bold(in string) string {
	return "**" + in + "**"
}

// extractLastPart reduces the name for the feedback to the last part.
func extractLastPart(input string) string {
	re := regexp.MustCompile(`[^-/]+$`)
	input = strings.TrimRight(input, "-/")
	return re.FindString(input)
}
