package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/google/generative-ai-go/genai"
	"google.golang.org/api/option"
)

const (
	generationFail = "gemini failed to deliver\n"
	noKeyFound     = "---<noKey>---\n"
	systemPrompt   = "use the following details to write about a NSC, make the output 150-200 words"
	systemFormat   = "output format is MarkDown, wrap the lines around 120 characters, give the name and the alias as headline. Use bold markings for the facts that you have been given for generation"
)

func generateNSCText(base, setting string) string {
	key := os.Getenv("GEMINI_API_KEY")
	if key == "" {
		return noKeyFound + base
	}
	ctx := context.Background()
	client, err := genai.NewClient(ctx, option.WithAPIKey(key))
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	model := client.GenerativeModel("gemini-1.5-flash")
	model.GenerationConfig.SetTemperature(1)
	model.SafetySettings = adultsOnly()
	model.SystemInstruction = &genai.Content{
		Parts: []genai.Part{
			genai.Text(systemPrompt),
			genai.Text(systemFormat),
			genai.Text(setting),
		},
	}
	resp, err := model.GenerateContent(ctx, genai.Text(base))
	if err != nil {
		log.Println(err)
		return generationFail + base
	}

	return printResponse(resp)
}

// This a background generated for an RPG for adults and characters will not
// reflect life is we cut out because we are shy .. more over it feels unclear
// how those categories should even show up with the requests for background.
// nolint -- this is not commented out code, but error format message
func adultsOnly() []*genai.SafetySetting {
	// HarmCategory.HARM_CATEGORY_HATE_SPEECH
	// HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT
	// HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT
	// HarmCategory.HARM_CATEGORY_HARASSMENT
	return []*genai.SafetySetting{
		{
			Category:  genai.HarmCategoryHateSpeech,
			Threshold: genai.HarmBlockNone,
		},
		{
			Category:  genai.HarmCategorySexuallyExplicit,
			Threshold: genai.HarmBlockNone,
		},
		{
			Category:  genai.HarmCategoryDangerousContent,
			Threshold: genai.HarmBlockNone,
		},
		{
			Category:  genai.HarmCategoryHarassment,
			Threshold: genai.HarmBlockNone,
		},
	}
}

func printResponse(resp *genai.GenerateContentResponse) string {
	var generated string
	for _, cand := range resp.Candidates {
		if cand.Content != nil {
			for _, part := range cand.Content.Parts {
				generated += fmt.Sprintf("%s", part)
			}
			return generated
		}
	}
	return "---"
}
