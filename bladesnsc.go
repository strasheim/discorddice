package main

import (
	"fmt"
)

func bladesNonPlayerCharacter(bd map[string][]string) string {
	output := "```\n"
	output += fmt.Sprintln("Name:     ", nscLookup(bd["name"]), nscLookup(bd["family"]), "---", nscLookup(bd["alias"]))
	output += fmt.Sprintln("Gender:   ", nscLookup(bd["gender"]))
	output += fmt.Sprintln("Heritage: ", nscLookup(bd["heritage"]))
	output += fmt.Sprintln("Looks:    ", nscLookup(bd["looks"]))
	output += fmt.Sprintln("Goal:     ", nscLookup(bd["goal"]), "via", nscLookup(bd["method"]))
	output += fmt.Sprintln("Trait:    ", nscLookup(bd["trait"]))
	output += fmt.Sprintln("Interest: ", nscLookup(bd["interests"]))
	output += fmt.Sprintln("Quirks:   ", nscLookup(bd["quirks"]))
	output += "```\n"
	return generateNSCText(output, "The setting is Blades in the Dark")
}
