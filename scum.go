package main

import (
	"fmt"
)

func scumNonPlayerCharacter(sd map[string][]string) string {
	output := "```\n"
	output += fmt.Sprintln("Name:     ", nscLookup(sd["alias"]), nscLookup(sd["family"]))
	output += fmt.Sprintln("Gender:   ", nscLookup(sd["gender"]))
	output += fmt.Sprintln("Heritage: ", nscLookup(sd["heritage"]))
	output += fmt.Sprintln("Looks:    ", nscLookup(sd["looks"]))
	output += fmt.Sprintln("Goal:     ", nscLookup(sd["goal"]), "via", nscLookup(sd["method"]))
	output += fmt.Sprintln("Interest: ", nscLookup(sd["interests"]))
	output += fmt.Sprintln("Quirks:   ", nscLookup(sd["quirks"]))
	output += "```\n"
	return generateNSCText(output, "The setting is Scum and Villainy")
}
