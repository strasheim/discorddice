package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strings"

	lzstring "github.com/daku10/go-lz-string"
)

type Character struct {
	CharName       string `json:"charName"`
	StringNotes    string `json:"stringNotes"`
	ConditionNotes string `json:"conditionNotes"`
}

func (b *bot) callouts() string {
	allChannels, err := b.s.GuildChannels(b.m.GuildID)
	if err != nil {
		log.Println("callouts", err)
		return ""
	}
	for _, c := range allChannels {
		if c.Name == "strings" {
			pinnedMessages, _ := b.s.ChannelMessagesPinned(c.ID)
			var collection string
			for _, pinned := range pinnedMessages {
				if strings.HasPrefix(pinned.Content, "https://") {
					fragment := strings.Split(pinned.Content, "#")[1]
					char, err := decodeFragment(fragment)
					if err != nil {
						log.Println("character decoding:", err)
						continue
					}
					collection += char.PrintYAML() + "\n"
				}
			}
			return collection
		}
	}
	return ""
}

func decodeFragment(s string) (*Character, error) {
	decodedFragment, err := url.QueryUnescape(s)
	if err != nil {
		return nil, fmt.Errorf("failed to decode URI component: %w", err)
	}

	decompressedData, err := lzstring.DecompressFromEncodedURIComponent(decodedFragment)
	if err != nil {
		return nil, fmt.Errorf("failed to decompress data: %w", err)
	}

	var character Character
	err = json.Unmarshal([]byte(decompressedData), &character)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling JSON: %w", err)
	}
	return &character, nil
}

// PrintYAML formats the Character struct as a YAML string.
func (c *Character) PrintYAML() string {
	var builder strings.Builder
	builder.WriteString(fmt.Sprintf("### %s\n", c.CharName))
	builder.WriteString("Strings:\n")
	builder.WriteString(formatAsList(c.StringNotes))
	builder.WriteString("Conditions:\n")
	builder.WriteString(formatAsList(c.ConditionNotes))
	return builder.String()
}

// Helper function to format a string with \r\n into a YAML list.
func formatAsList(s string) string {
	var builder strings.Builder
	lines := strings.Split(s, "\r\n")
	for _, line := range lines {
		if line != "" {
			builder.WriteString(fmt.Sprintf("- %s\n", line))
		}
	}
	return builder.String()
}
