## Golang Build Container
FROM registry.gitlab.com/strasheim/go-container AS builder

WORKDIR /go/b
COPY . .
COPY .golangci.yml .golangci.yml
ENV GOARCH=amd64 GOOS=linux CGO_ENABLED=0
RUN go build -ldflags "-s -w -extldflags \"-static\""
RUN golangci-lint run
RUN go test -v 


## Application Container
FROM scratch 

COPY --from=builder /go/b/discorddice discorddice
COPY --from=builder /etc/ssl/certs /etc/ssl/certs/
ADD scum /scum
ADD blades /blades

USER 65534
# ENV BOTTOKEN needs to be set on the docker host 
CMD  ["/discorddice"]
