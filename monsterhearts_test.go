package main

import (
	"testing"
)

func TestExtractLastPart(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{"example-string", "string"},
		{"example string", "example string"},
		{"path/to/resource", "resource"},
		{"no-special-char", "char"},
		{"simpleword", "simpleword"},
		{"another/example/", "example"},
		{"trailing-slash/", "slash"},
		{"just-a-single-dash-", "dash"},
	}

	for _, test := range tests {
		result := extractLastPart(test.input)
		if result != test.expected {
			t.Errorf("For input '%s', expected '%s', but got '%s'", test.input, test.expected, result)
		}
	}
}
