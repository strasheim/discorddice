package main

import (
	"regexp"
	"strings"
	"testing"
)

func TestWorkDice(t *testing.T) {
	tCase := []struct {
		line   string
		expect string
	}{{
		line:   "4d6+4",
		expect: "testing",
	}, {
		line:   "Fnord",
		expect: "",
	}}

	for _, c := range tCase {
		if !strings.Contains(workDice(c.line, "testing"), c.expect) {
			t.Errorf("%s was given and did not result in: %s\n", c.line, c.expect)
		}
	}
}

func TestGermanize(t *testing.T) {
	change := germanize("2d6")
	stay := germanize("2d")
	if change == "2d6" || stay == "2w" {
		t.Errorf("Changing dice letter to a german w (%v) [2w6] failed\n Maybe the greed is real (%v) [2d]\n", change, stay)
	}
}

func TestHeart(t *testing.T) {
	tCase := []struct {
		line   string
		expect string
	}{{
		line:   "heart 3",
		expect: "testing",
	}, {
		line:   "heart 3 r",
		expect: `\d+ \d+ `,
	}, {
		line:   "heart 3 d",
		expect: `\d+ `,
	}}

	for _, c := range tCase {
		line := heart(c.line, "justtesting")
		if !regexp.MustCompile(c.expect).MatchString(line) {
			t.Errorf("Heart dice  roll should have rolled: `%v`\n", line)
		}
	}
}

func TestDup(t *testing.T) {
	// checks if in the slice there are dup entries
	istrue := dup([]string{"hallo", "allo", "hallo"})
	isfalse := dup([]string{"hallo", "allo"})
	if isfalse != false || istrue != true {
		t.Errorf("Duplication detection failed\n")
	}
}

func TestTokenize(t *testing.T) {
	msg := "3w6+3-2+5w7"
	tokens := tokenize(msg)
	if tokens[0] != "3w6" ||
		tokens[1] != "+" ||
		tokens[2] != "3" ||
		tokens[3] != "-" ||
		tokens[4] != "2" ||
		tokens[5] != "+" ||
		tokens[6] != "5w7" {
		t.Errorf("Tokens not correctly found: in `%v` out `%v`\n", msg, tokens)
	}
}

func TestNegativeTokens(t *testing.T) {
	msg := "-4w6-10"
	tokens := tokenize(msg)
	if tokens[0] != "" ||
		tokens[1] != "-" ||
		tokens[2] != "4w6" ||
		tokens[3] != "-" ||
		tokens[4] != "10" {
		t.Errorf("Tokens not correctly found: in `%v` out `%v`\n", msg, tokens)
	}
}

func TestNormilize(t *testing.T) {
	chat := "-1w6"
	cleaned := normalize(chat)
	if cleaned != "-w6" {
		t.Errorf("Normalization failed: in `%v` out `%v` [%b]\n", chat, cleaned, []byte(cleaned))
	}
	chat = "-4w6"
	cleaned = normalize(chat)
	if cleaned != "-w6-w6-w6-w6" {
		t.Errorf("Normalization failed: in `%v` out `%v`\n", chat, cleaned)
	}
	chat = "4w6"
	cleaned = normalize(chat)
	if cleaned != "+w6+w6+w6+w6" {
		t.Errorf("Normalization failed: in `%v` out `%v`\n", chat, cleaned)
	}
}

func TestProst(t *testing.T) {
	worker := prost("prost")
	if !strings.Contains(worker, " ") {
		t.Errorf("Input should be valid - \n%v\n", worker)
	}
}

func TestWorkFate(t *testing.T) {
	tCase := []struct {
		line   string
		expect string
	}{{
		line:   "fate",
		expect: "justtesting#",
	}, {
		line:   "fate4",
		expect: "justtesting#",
	}, {
		line:   "fate 4",
		expect: "justtesting#",
	}}

	for _, c := range tCase {
		output := workFate(c.line, "justtesting")
		if !strings.Contains(output, c.expect) {
			t.Errorf("fate role %v should work for: `%v`\n", c.line, output)
		}
	}
}

// nolint - max is function, yet here its just a float
func TestD6(t *testing.T) {
	sum := 0.0
	max := 10000.0

	for i := 0; i < int(max); i++ {
		sum += float64(d6())
	}
	if sum <= max*3.5-500 || sum >= max*3.5+500 {
		t.Errorf("Dice roll appears off - %f \n", sum)
	}

	sum = 0.0
	max = 10000.0
	q := "<2"
	for i := 0; i < int(max); i++ {
		sum += float64(d6(q))
	}
	if sum <= (max*3.5)/2-500 || sum >= (max*3.5)/2+500 {
		t.Errorf("Loaded dice roll appears off - %f \n", sum)
	}

	sum = 0.0
	max = 10000.0
	p := "2"
	for i := 0; i < int(max); i++ {
		sum += float64(d6(p))
	}
	if sum <= max*3.5*1.5-500 || sum >= max*3.5*1.5+500 {
		t.Errorf("Loaded dice roll appears off - %f \n", sum)
	}
}

func TestWorkDark(t *testing.T) {
	tCase := []struct {
		line   string
		expect string
	}{{
		line:   "dark",
		expect: "justtesting#",
	}, {
		line:   "dark4",
		expect: "justtesting#",
	}}

	for _, c := range tCase {
		output := workDark(c.line, "justtesting")
		if !strings.Contains(output, c.expect) {
			t.Errorf("dark role %v should work for: `%v`\n", c.line, output)
		}
	}
}

func TestSuccessLevel(t *testing.T) {
	tCase := []struct {
		level     []int
		skillrole bool
		result    string
	}{{
		level:     []int{1, 2, 3, 4},
		skillrole: true,
		result:    "```autohotkey\n% ROLL%\n```",
	}, {
		level:     []int{6, 6},
		skillrole: true,
		result:    "```diff\n+ ROLL !!!\n```",
	}, {
		level:     []int{5, 6},
		skillrole: true,
		result:    "```diff\n+ ROLL\n```",
	}, {
		level:     []int{2, 3},
		skillrole: true,
		result:    "```diff\n- ROLL\n```",
	}, {
		level:     []int{1, 6},
		skillrole: false,
		result:    "```diff\n- ROLL\n```",
	}, {
		level:     []int{6, 6},
		skillrole: false,
		result:    "```diff\n+ ROLL\n```",
	}, {
		level:     []int{4, 6},
		skillrole: false,
		result:    "```autohotkey\n% ROLL%\n```",
	}}

	for _, c := range tCase {
		output := successLevel(c.level, c.skillrole)
		if output != c.result {
			t.Errorf("Dark color code incorrect for %v, got:\n%s\n instead of:\n%s\n", c.level, output, c.result)
		}
	}
}
